# what is useEffect, what is useEffect used for?
## what is useEffect:
- a hook that allows you to perform side effects in functional components. 
- lets you synchronize a component with an external system.
- useEffect(setup, dependencies?)
    - setup: a function that will run when the component mounts or updates.
    - dependencies: an array of dependencies that will cause the setup function to run when they change. When no dependencies are provided ``[]``, the setup function will run only once.
## what is useEffect used for?
- Connecting to an external system (e.g SQL)
- Updating state Wrapping Effects in custom Hooks 
- Controlling a non-React widget (e.g. a timer, map)
- Fetching data with Effects (like async/await API)
- Specifying reactive dependencies 
- Updating state based on previous state from an Effect (useState)
- Removing unnecessary object dependencies 
- Removing unnecessary function dependencies 
- Reading the latest props and state from an Effect
- Displaying different content on the server and the client
    

