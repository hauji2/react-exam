export const Car = () => {
    const brands = ['Ford', 'BMW', 'Audi'];
    return (
        <div>
            <h1>Who lives in my garage?</h1>
            {brands.map((brand) => <ul><li>I am a {brand}</li></ul>)}
        </div>
    )
};