import { useState } from "react";

export const Hello = () => {
    const [name, setName] = useState('');

    return (
        <div>
            <p>Enter your name: {name}</p>
            <input type="text" id="name"/>
            <button onClick={() => setName(document.getElementById('name').value)}>Submit</button>
        </div>
    )
};
