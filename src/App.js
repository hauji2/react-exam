import './App.css';
import { Car } from './Car';
import { Hello } from './Hello';

function App() {
  return (
    <div className="App">
      <Hello />
      <Car />
    </div>
  );
}

export default App;
